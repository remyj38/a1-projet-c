#ifndef AFFICHAGE_H_INCLUDED
#define AFFICHAGE_H_INCLUDED
#include "actions.h"


int afficherCombinaisons(carton** combinaisons, int nombreCartons);
void afficherPage(carton combinaisons[]);
void afficherTotal(carton combinaisons[]);


#endif // AFFICHAGE_H_INCLUDED
