#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED

typedef struct carton carton;
struct carton {
    int numeroSerie;
    int ligne1[9];
    int ligne2[9];
    int ligne3[9];
    char* combinaison;
};
carton** recupCartons(int lot, FILE* fichier, int* nombreCarton);
void explode(carton* cartons);
#endif // ACTIONS_H_INCLUDED
