#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "affichage.h"
#include "actions.h"

void explode(carton* carton) {
    int i, j=0, k=0, l=0;
    char temp[100];
    int* tableau;
    tableau = calloc(28, sizeof(int));
    for (i=0; i<strlen(carton->combinaison); i++) {
        if (carton->combinaison[i] != ':' && carton->combinaison[i] != '\\') {
            temp[j] = carton->combinaison[i];
            j++;
        } else {
            if (carton->combinaison[i-1] == ':') {
                tableau[k] = 0;
            } else {
            sscanf(temp, "%d", &tableau[k]);
            }
            k++;
            j=0;
            for (l=0; l<strlen(temp); l++) {
                temp[l]="\0";
            }
        }
    }
    sscanf(temp, "%d", &tableau[k]);
    carton->numeroSerie = tableau[0];
    for(i=0; i<9; i++) {
        carton->ligne1[i] = tableau[i+1];
    }
    for(i=0; i<9; i++) {
        carton->ligne2[i] = tableau[i+10];
    }
    for(i=0; i<9; i++) {
        carton->ligne3[i] = tableau[i+19];
    }
        free(tableau);
}
carton** recupCartons(int lot, FILE* fichier, int* nombreCarton)
{
    char nomFichier[20];
    char chaine[100];
    int i;
    carton** cartons;
    sprintf(nomFichier, "%d.lot", lot);
    fichier = fopen(nomFichier, "r");
    if (fichier == NULL) {
        printf("Le lot selectionne n'existe pas !");
        exit(0);
    }
    fgets(chaine, 100, fichier);
    sscanf(strchr(chaine, ':')+1, "%d", nombreCarton);
    cartons = calloc(*nombreCarton, sizeof(carton));
    for (i=0; i<*nombreCarton; i++) {
        cartons[i]=malloc(sizeof(carton));
    }
    for (i=0; fgets(chaine, 100, fichier); i++) {
        cartons[i]->combinaison = calloc(strlen(chaine), sizeof(char));
        strcpy(cartons[i]->combinaison, chaine);
        explode(cartons[i]);
    }
    return cartons;

}
