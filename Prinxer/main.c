#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"
#include "affichage.h"


int main()
{
    int lot;
    carton** cartons;
    FILE* fichier;
    printf("Quel lot voulez vous afficher ?\n");
    int* nombreCarton;
    nombreCarton = malloc(sizeof(int));
    while(!scanf("%d", &lot)) //Tant que la saisie n'est pas un nombre, on recommence
    {
        printf("Merci d'entrer un nombre ! \nReessayez : ");
        fflush(stdin);
    }
    cartons=recupCartons(lot, fichier, nombreCarton);
    afficherCombinaisons(cartons, *nombreCarton);
    return 0;
}
