#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "affichage.h"
#include "actions.h"

int afficherCombinaisons(carton** combinaisons, int nombreCartons)
{
    int i ,j , k, mini, maxi;

    do
    {
        printf("Quelle est la premiere combinaison que vous souhaitez afficher ?\n");
        while(!scanf("%d", &mini)) //Tant que la saisie n'est pas un nombre, on recommence
        {
            printf("Merci d'entrer un nombre ! \nReessayez : ");
            fflush(stdin);
        }
        printf("Quelle est la derniere combinaison que vous souhaitez afficher ?\n");
        while(!scanf("%d", &maxi)) //Tant que la saisie n'est pas un nombre, on recommence
        {
            printf("Merci d'entrer un nombre ! \nReessayez : ");
            fflush(stdin);
        }
        if (maxi>nombreCartons)
        {
            printf("Le lot contient %d cartons. Merci d'entrer votre intervale en vous adaptatant au nombre de cartons.\n", nombreCartons);
        }
    }
    while (maxi>nombreCartons);
    for (k=mini-1; k<maxi; k++)
    {
        system("cls");
        printf("Combinaison du carton %d : %s\n", k+1, combinaisons[k]->combinaison+4);
        printf("%c", 218);
        for (i = 0 ; i < 8 ; i++)
        {
            printf("%c%c%c", 196 ,196 ,194);
        }
        printf("%c%c%c\n", 196, 196, 191);
        for (j = 0 ; j < 3 ; j++)
        {
            if (j == 0)
            {
                afficherNombre (combinaisons[k]->ligne1);
            }
            if (j == 1)
            {
                afficherNombre (combinaisons[k]->ligne2);
            }
            if (j == 2)
            {
                afficherNombre (combinaisons[k]->ligne3);
            }
            if (j > 1)
            {
                printf("%c\n%c",179 ,192);
                for (i = 0 ; i < 8 ; i++)
                {
                    printf("%c%c%c", 196 ,196 ,193);
                }
                printf("%c%c%c", 196 ,196 ,217);
            }
            else
            {
                printf("%c\n%c", 179, 195);
                for (i = 0 ; i < 8 ; i++)
                {
                    printf("%c%c%c", 196 ,196 ,197);
                }
                printf("%c%c%c\n",196 ,196 ,180);
            }
        }
        system("pause");
    }
}

void afficherNombre (int tableau1[])
{
    int i;
    for (i = 0 ; i < 9 ; i++)
    {
        if (tableau1[i]< 10)
        {
            if ( tableau1[i] == 0)
            {
                printf("%c%c ", 179, 178);
            }
            else
            {
                printf("%c%d ", 179, tableau1[i]);
            }

        }
        else
        {
            printf("%c%d", 179, tableau1[i]);
        }

    }

}
