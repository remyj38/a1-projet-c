#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "affichage.h"
#include "actions.h"

int main()
{
    // D�finition des variables
    FILE *fichier = NULL;
    int nombreCartons = 0;
    int lot = time(NULL);
    int nombreManuels =0;
    char flot[20];
    char** combinaisons;
    sprintf(flot, "%d.lot", lot);

    fichier = fopen(flot, "a+");                                                 // ouverture du fichier en mode ajout : curseur plac� � la fin du fichier, si le fichier inexistant : cr�ation du fichier
    if (fichier == NULL)                                                         // Si l'ouverture du fichier echoue, on arrete le programme en signallant que l'ouverture � �chou�e
    {
        printf("erreur lors de l'ouverture du fichier de stockage des cartons");
    }
    else                                                                         // Sinon, on continue le programme
    {
        printf("Combien de carton voulez-vous generer ? (si 0 ou inferieur, 500 enregistre)\n");                     //On demande le nombre de cartons que l'utilisateur veut g�n�rer
        while(!scanf("%d", &nombreCartons))                                      //Tant que la saisie n'est pas un nombre, on recommence
        {
            printf("Merci d'entrer un nombre ! \nReessayez : ");
            fflush(stdin);
        }
        if (nombreCartons <=0)                                               // Si le nombre de carton saisit est nul ou n�gatif, on signale l'utilisateur, et on lui redemande un nombre de carton
        {
            nombreCartons = 500;
        }
        combinaisons = (char**) calloc(nombreCartons, sizeof(char*));
        fprintf(fichier, "%d:%d\n", lot, nombreCartons);                 // On �crit dans le fichier le n� du lot et le nombre de cartons � g�n�rer
        nombreManuels = generationManuelle(combinaisons, fichier);       // On execute la fonction permettant de demander le nombre de cartons a rentrer manuellement
        genererAleatoire(nombreCartons, nombreManuels, combinaisons, fichier); // On g�n�re les cartons restant de maniere al�atoire
        afficherCombinaisons(combinaisons, nombreCartons);               // On affiche les combinaisons g�n�r�es*/
    }
    fclose(fichier);
    system("pause");
    return 0;
}
