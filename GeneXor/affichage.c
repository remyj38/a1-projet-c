#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "actions.h"
#include "affichage.h"

void afficherTrous(char** combinaisons, int taille)
{
    int i, j;
    for (i=0; i<taille/sizeof(char); i++)
    {
        for(j=0; j<strlen(combinaisons[i]); j++)
        {
            if (combinaisons[i][j] == ':')
            {
                printf(":");
                if ((combinaisons[i][j+1] == ':') || (!combinaisons[i][j+1]))
                {
                    printf("%c", (char)219);
                }
            }
            else
            {
                printf("%c", combinaisons[i][j]);
            }
        }
        printf("\n");
    }
}


void afficherCombinaisons(char** combinaisons, int tailleTableau)
{
    int choixAffichage = 0 , i;
    printf ("vous avez le choix entre afficher les combinaisons avec ou sans les trous .\n pour afficher avec les trou entrer 1\n pour afficher sans les trou entrer 2 :\n");
    while(!scanf("%d", &choixAffichage))
    {
        printf("Merci d'entrer un nombre ! \nReessayez : ");
        fflush(stdin);
    }
    if ( choixAffichage == 1 )
    {
        printf( "affichage avec trou : \n");
        afficherTrous(combinaisons,tailleTableau);

    }
    if ( choixAffichage == 2)
    {
        printf( "affichage sans trou : \n");

        for (i=0 ; i<tailleTableau; i++)
        {
            printf("%s\n", combinaisons[i]);
        }
    }
}

void genererAleatoire(int nombreCartons, int nombreManuels, char** combinaisons , FILE* fichier)
{
    int i;
    for (i=nombreManuels+1; i<=nombreCartons; i++)
    {
        if (!genererCombiAuto(i, combinaisons, fichier))
        {
            i--;
        }
    }
}
int generationManuelle(char** combinaisons, FILE* fichier)
{
    int nbCartonManu=500, i;
    printf("Combien voulez vous de carton a entrer manuellement?\n La valeur par defaut est de 500 Cartons\n");
    while(!scanf("%d", &nbCartonManu)) //Tant que la saisie n'est pas un nombre, on recommence
    {
        printf("Merci d'entrer un nombre ! \nReessayez : ");
        fflush(stdin);
    }
    for(i=1; i<=nbCartonManu; i++)
    {
        genererManuel(combinaisons, fichier, i);
    }
    return nbCartonManu;
}
