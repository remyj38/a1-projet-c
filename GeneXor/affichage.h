void afficherCombinaisons(char** combinaisons, int tailleTableau);
void afficherTrous(char** combinaisons, int taille);
int generationManuelle(char** combinaisons, FILE* fichier);
void genererAleatoire(int nombreCartons, int nombreManuels, char** combinaisons, FILE* fichier);

