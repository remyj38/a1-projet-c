int genererManuel(char** combinaisons, FILE* fichier, int numeroCombinaison);
int genererCombiAuto(int numeroCombinaison, char** combinaisons,FILE* fichier);
int verifCoformManu(int ligne1[], int ligne2[], int ligne3[]);
int verifDoublons(char** combinaisons, char* aTester, int numeroCombinaison);
char* convertionChaine(int ligne1[], int ligne2[], int ligne3[]);
int enregistrerCombinaison(int numeroCombinaison, char combinaison[], FILE* fichier);
char* ajoutTrous(int ligne1[], int ligne2[], int ligne3[]);
