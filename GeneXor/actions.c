#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "affichage.h"
#include "actions.h"

int enregistrerCombinaison(int numeroCombinaison, char combinaison[], FILE* fichier)
{
    if (fichier != NULL)
    {
        fprintf(fichier, "%d:%s\n", numeroCombinaison, combinaison);
        return 1;
    }
    else
    {
        return 0;
    }
}
int genererCombiAuto(int numeroCombinaison, char** combinaisons,FILE* fichier)
{
    int i;
    int tabLigne1[9];
    int tabLigne2[9];
    int tabLigne3[9];
    char* chaine;
    srand (time (NULL));

    for (i=0; i<9; i++)
    {
        switch (i)
        {
        case 0:
            tabLigne1 [0] = rand()%8+1;
            tabLigne2 [0] = rand()%8+1;
            while (tabLigne2[0] == tabLigne1[0])
            {
                tabLigne2 [0] = rand()%8+1;
            }
            tabLigne3 [0] = rand()%8+1;
            while (tabLigne3[0] == tabLigne1[0] || tabLigne3[0] == tabLigne2[0])
            {
                tabLigne3 [0] = rand()%8+1;
            }
            break;
        case 8 :
            tabLigne1 [8] = rand()%10+80;
            tabLigne2 [8] = rand()%10+80;
            while (tabLigne2[8] == tabLigne1[8])
            {
                tabLigne2 [8] = rand()%10+80;
            }
            tabLigne3 [8] = rand()%10+80;
            while (tabLigne3[8] == tabLigne1[8] || tabLigne3[8] == tabLigne2[8])
            {
                tabLigne3 [8] = rand()%10+80;
            }
            break;
        default :
            tabLigne1 [i] = rand ()%10+(10*i);
            tabLigne2 [i] = rand ()%10+(10*i);
            while (tabLigne2[i] == tabLigne1[i])
            {
                tabLigne2 [i] = rand ()%10+(10*i);
            }
            tabLigne3 [i] = rand ()%10+(10*i);
            while (tabLigne3[i] == tabLigne1[i] || tabLigne3[i] == tabLigne2[i])
            {
                tabLigne3 [i] = rand ()%10+(10*i);
            }
            break;
        }
    }
    chaine = ajoutTrous(tabLigne1, tabLigne2, tabLigne3);
    if (!verifDoublons(combinaisons, chaine, numeroCombinaison))
    {
        enregistrerCombinaison(numeroCombinaison, chaine, fichier);
        return 1;
    }
    else
    {
        return 0;
        free(chaine);
    }
}
char* convertionChaine(int ligne1[], int ligne2[], int ligne3[])
{
    int i;
    char chaine[100];
    char* chaineOK;
    sprintf(chaine, "%d", ligne1[0]);
    for (i=1; i<9; i++)
    {
        if (ligne1[i])
        {
            sprintf(chaine, "%s:%d", chaine, ligne1[i]);
        }
        else
        {
            strcat(chaine, ":");
        }
    }
    for (i=0; i<9; i++)
    {
        if (ligne2[i])
        {
            sprintf(chaine, "%s:%d", chaine, ligne2[i]);
        }
        else
        {
            strcat(chaine, ":");
        }
    }
    for (i=0; i<9; i++)
    {
        if (ligne3[i])
        {
            sprintf(chaine, "%s:%d", chaine, ligne3[i]);
        }
        else
        {
            strcat(chaine, ":");
        }
    }
    chaineOK = (char*) calloc(strlen(chaine), sizeof(char));
    strcpy(chaineOK, chaine);
    return chaineOK;
}

char* ajoutTrous(int ligne1[], int ligne2[], int ligne3[])
{
    int MIN = 0 , MAX = 8, i, valeurTampon, tableauTrous [4] = {10,10,10,10};
    srand(time(NULL)); // permet de générer des nombre aléatoires en fonction des millisecondes
    for (i = 0; i < 4 ; i++)
    {
        valeurTampon = (rand() % (MAX - MIN + 1)) + MIN; // tire une valeur random et la place dans une valeur tampon

        if (valeurTampon != tableauTrous[0] && valeurTampon != tableauTrous[1] && valeurTampon != tableauTrous[2] && valeurTampon != tableauTrous[3])
            // compare la valeur tampon pour savoir si elle est deja presente dans le tableau
        {
            tableauTrous[i] = valeurTampon; // si elle n'est pas dans le tableau on la place dans la case correspondante
        }
        else
        {
            i--; // sinon on recommence le random de ce nombre la
        }
    }
    for (i = 0; i < 4 ; i++)
    {
        ligne1 [tableauTrous [i] ] = 0 ; /* on place le 0 dans la case qui correspond au différente valeur présente dans les case du premier tableau */
        tableauTrous[i]= 10;
    }
    for (i = 0; i < 4 ; i++)
    {
        valeurTampon = (rand() % (MAX - MIN + 1)) + MIN; // tire une valeur random et la place dans une valeur tampon

        if (valeurTampon != tableauTrous[0] && valeurTampon != tableauTrous[1] && valeurTampon != tableauTrous[2] && valeurTampon != tableauTrous[3])
            // compare la valeur tampon pour savoir si elle est deja presente dans le tableau
        {
            tableauTrous[i] = valeurTampon; // si elle n'est pas dans le tableau on la place dans la case correspondante
        }
        else
        {
            i--; // sinon on recommence le random de ce nombre la
        }
    }
    for (i = 0; i < 4 ; i++)
    {
        ligne2 [tableauTrous [i] ] = 0 ; /* on place le 0 dans la case qui correspond au différente valeur présente dans les case du premier tableau */
        tableauTrous[i]= 10;
    }
    for (i = 0; i < 4 ; i++)
    {
        valeurTampon = (rand() % (MAX - MIN + 1)) + MIN; // tire une valeur random et la place dans une valeur tampon

        if (valeurTampon != tableauTrous[0] && valeurTampon != tableauTrous[1] && valeurTampon != tableauTrous[2] && valeurTampon != tableauTrous[3])
            // compare la valeur tampon pour savoir si elle est deja presente dans le tableau
        {
            tableauTrous[i] = valeurTampon; // si elle n'est pas dans le tableau on la place dans la case correspondante
        }
        else
        {
            i--; // sinon on recommence le random de ce nombre la
        }
    }
    for (i = 0; i < 4 ; i++)
    {
        ligne3 [tableauTrous [i] ] = 0 ; /* on place le 0 dans la case qui correspond au différente valeur présente dans les case du premier tableau */
    }
    return convertionChaine(ligne1, ligne2, ligne3);
}

int verifDoublons(char** combinaisons, char* aTester, int numeroCombinaison)
{
    int i;
    int test=0;
    if (numeroCombinaison!=1)
    {
        for (i=0; i<numeroCombinaison-1; i++)
        {
            if (!strcmp(combinaisons[i], aTester))
            {
                test=1;
            }
        }
    }
    if (test)
    {
        return 1;
    }
    else
    {
        combinaisons[numeroCombinaison-1] = aTester;
        return 0;
    }
}


int verifCoformManu(int ligne1[], int ligne2[], int ligne3[])
{
    int i;
    int l1 = 0, l2 = 0, l3 = 0;
    for (i=0; i<9; i++)
    {
        if (!ligne1[i])
        {
            l1++;
        }
    }
    for (i=0; i<9; i++)
    {
        if (!ligne2[i])
        {
            l2++;
        }
    }
    for (i=0; i<9; i++)
    {
        if (!ligne3[i])
        {
            l3++;
        }
    }
    if (l1==4 && l2==4 && l3==4)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
int genererManuel(char** combinaisons, FILE* fichier, int numeroCombinaison)
{
    int numColonne, i, mini, maxi;
    int tabLigne1[9];
    int tabLigne2[9];
    int tabLigne3[9];
    char* chaine;

    for(i=0; i<9; i++)
    {
        tabLigne1[i]=0;
        tabLigne2[i]=0;
        tabLigne3[i]=0;
    }

    for (numColonne=0; numColonne<9; numColonne++)
    {
        switch (numColonne)
        {
        case 0:
            mini =1;
            maxi =9;
            break;
        case 8 :
            mini = 80;
            maxi = 90;
            break;
        default :
            mini = 10*numColonne;
            maxi = 10*numColonne+9;
            break;
        }
        do
        {
            printf("Entrer le premier nombre de la colonne %d (compris entre %d et %d)\n", numColonne+1, mini, maxi);
            while(!scanf("%d", &tabLigne1[numColonne])) //Tant que la saisie n'est pas un nombre, on recommence
            {
                printf("Merci d'entrer un nombre ! \nReessayez : ");
                fflush(stdin);
            }
        }
        while (!(tabLigne1[numColonne] >= mini && maxi >= tabLigne1[numColonne]) && tabLigne1[numColonne] != 0);
        do
        {
            printf("Entrer le second nombre de la colonne %d (compris entre %d et %d)\n", numColonne+1, mini, maxi);
            while(!scanf("%d", &tabLigne2[numColonne])) //Tant que la saisie n'est pas un nombre, on recommence
            {
                printf("Merci d'entrer un nombre ! \nReessayez : ");
                fflush(stdin);
            }
        }
        while (!(tabLigne2[numColonne] >= mini && maxi >= tabLigne2[numColonne]) && tabLigne2[numColonne] != 0);
        while (tabLigne2[numColonne] == tabLigne1[numColonne] && tabLigne2[numColonne] != 0)
        {
            printf("Le second nombre de la colonne ne doit pas etre identique au premier.\n");
            printf("Entrer le second nombre de la colonne %d (compris entre %d et %d)\n", numColonne+1, mini, maxi);
            while(!scanf("%d", &tabLigne2[numColonne])) //Tant que la saisie n'est pas un nombre, on recommence
            {
                printf("Merci d'entrer un nombre ! \nReessayez : ");
                fflush(stdin);
            }
        }
        do
        {
            printf("Entrer le troisieme nombre de la colonne %d (compris entre %d et %d)\n", numColonne+1, mini, maxi);
            while(!scanf("%d", &tabLigne3[numColonne])) //Tant que la saisie n'est pas un nombre, on recommence
            {
                printf("Merci d'entrer un nombre ! \nReessayez : ");
                fflush(stdin);
            }
        }
        while (!(tabLigne3[numColonne] >= mini && maxi >= tabLigne3[numColonne]) && tabLigne3[numColonne] != 0);
        while ((tabLigne3[numColonne] == tabLigne1[numColonne] || tabLigne3[numColonne] == tabLigne2[numColonne]) && tabLigne3[numColonne] !=0)
        {
            printf("Le troisieme nombre ne doit pas etre identique a un des deux premiers.\n");
            printf("Entrer le troisieme nombre de la colonne %d (compris entre %d et %d)\n", numColonne+1, mini, maxi);
            while(!scanf("%d", &tabLigne3[numColonne])) //Tant que la saisie n'est pas un nombre, on recommence
            {
                printf("Merci d'entrer un nombre ! \nReessayez : ");
                fflush(stdin);
            }
        }
    }
    if (verifCoformManu(tabLigne1, tabLigne2, tabLigne3))
    {
        chaine = convertionChaine(tabLigne1, tabLigne2, tabLigne3);
        if (!verifDoublons(combinaisons, chaine, numeroCombinaison))
        {
            enregistrerCombinaison(numeroCombinaison, chaine, fichier);
            return 1;
        }
    }
    return 0;
}
